Seven Mountains
Blurbs for copy concepts so far:
Seven Mountain Fund!

75% off Value of Tokens!

Choose your Option and claim your Tokens on the next page after payment!

ADD 6 MAIN ART PIECES HERE WITH PRICES BELOW THEM AND THE WIDGET FROM STRIPE HERE

Flow:

==>Purchase Digital Token

----->  Customer pays  50,100, 250, 500, 1000 or 2500 for a token category <-- backend stores data for 2nd page -->

-->Website checks if the customer has Metamask/Web3 wallet installed in browser/phone.

If NO:

----->  Customer installs Metamask Chrome or MS Edge Browser or Phone app for use of Ethereum Blockchain Claim button

if YES:

----->  Customer is taken to 2nd page with a Single or Multiple claim buttons {depending on what is easier for the frontend Dev}

-->  2nd page has 4 or 1 button.

=> 1 button model:  Checks which collectable category they paid for.  Checks if payment was recieved by Stripe.  Gives them the proper claim amount of tokens based on their level of category.

=> 6 button model:  Checks Which Collectable they paid for.  Unlocks the button with the correct amount of tokens for their contribution level.

------------>  Metamask Popup to accept the claim.  Clients will be warned then need ETH in their wallet to claim the token and there will be a 
link to several options on how to get ETH into their wallet with FIAT.  [The swap site is a 2nd website unrelated to this one.]

==> Claim transaction is sent to blockchain.  After confirmation they will be told to press the button to Add the token to the wallet.  We have a widget for that already we just paste into the website frontend code.

===>  Now they can click their metamask icon on their browser puzzle piece extension list and it will show the 7MT tokens they reserved with their Digital Token they purchased for the 75% discount on the tokens Claimed on the 2nd page.
